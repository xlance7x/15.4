﻿#include <iostream>

void FindOddNumbers(int N, bool isOdd)
{
	for (int a = isOdd; a <=  N; a += 2) 
	{
		std::cout << a << " ";
	}
}

int main()
{

	FindOddNumbers(15, true);
	FindOddNumbers(20, false);

}
